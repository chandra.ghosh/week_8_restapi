from rest_framework import serializers
from qa.models import Question, Answer


class AnswerDetailSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Answer
        fields = ('user', 'question', 'content', 'votes')

        
class AnswerListSerializer(serializers.ModelSerializer):
    """ owner = serializers.ReadOnlyField(source='owner.username') """
    question = serializers.HyperlinkedRelatedField(
        lookup_field='pk',
        view_name='question-detail',
        read_only=True
    )
    
    class Meta:
        model = Answer
        fields = ('user', 'question', 'content', 'votes')


class QuestionSerializer(serializers.ModelSerializer):
    """ owner = serializers.ReadOnlyField(source='owner.username') """
    answers = AnswerDetailSerializer(many=True, read_only=True)
        
    class Meta:
        model = Question
        fields = ('id', 'title', 'description', 'answers')
        


