from django.urls import path
from qa import views


urlpatterns = [
    path('questions', views.QuestionList.as_view()),
    path(
        'questions/<int:pk>',
        views.QuestionDetail.as_view(),
        name='question-detail'
        ),
    path('questions/<int:pk>/answers', views.AnswerList.as_view()),   
]