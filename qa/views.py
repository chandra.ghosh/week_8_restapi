from qa.models import Question, Answer
from qa.serializers import QuestionSerializer, AnswerListSerializer
from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticatedOrReadOnly


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.user == request.user


class QuestionList(generics.ListCreateAPIView):

    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)
    queryset = Question.objects.all()    
    serializer_class = QuestionSerializer
    
    """ if(serializer_class.data['user'] != self.request.user): """


class AnswerList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Answer.objects.filter(question=pk)

    queryset = Answer.objects.all()
    serializer_class = AnswerListSerializer